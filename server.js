var express = require('express');
var app = express();
var compress = require('compression');
app.use(compress());
app.use(express.static(__dirname + '/public'));

var http = require('http').Server(app);
var io = require('socket.io')(http);
var escape = require('escape-html');
var participants = {};
var mongoUri = process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || 'mongodb://localhost/blackholechat';
var MongoClient = require('mongodb').MongoClient, format = require('util').format;
var db = MongoClient.connect(mongoUri, function(error, databaseConnection) {
	db = databaseConnection;
});

app.get('/', function(request, response) {
	response.sendFile(__dirname + '/index.html');
});

app.get('/last.json', function(request, response) {
	response.set('Content-Type', 'application/json');
	db.collection('messages', function(error, coll) {
		coll.find().sort({_id:-1}).limit(10).toArray(function(error, results) {
			response.send(results.reverse());
		});
	});
});

io.on('connection', function(socket) {
	io.emit('new connection', Object.keys(participants).length);
	socket.on('new user', function(username) {
		cleanedUpUsername = username.replace(/[^\w]/gi, '');  //https://stackoverflow.com/questions/4374822/javascript-regexp-remove-all-special-characters
		participants[socket.id] = cleanedUpUsername;

		// Sends new connected user his/her uid
		io.sockets.connected[socket.id].emit("uid", JSON.stringify({"username":cleanedUpUsername,"uid":socket.id}));
		
		// Update number of connected users
		io.emit('new connection', Object.keys(participants).length);
	});
	socket.on('chat message', function(msg) {
		parsed = JSON.parse(msg);
		// Make sure username and socket ID are known, legitimate
		if (participants[socket.id] == parsed.username) {
			// Check if we are dealing with a direct message
			if (parsed.content.charAt(0) == '@') {
				var otherUsername = parsed.content.substr(1, parsed.content.indexOf(' ') - 1);
				var directMessage = parsed.content.substr(parsed.content.indexOf(' ') + 1, parsed.content.length);
				directMessage.content = escape(parsed.directMessage);
				// Find socket ID of other username. THIS IS IMPERFECT!
				var otherUsernameFound = false;
				var otherSocketID;
				for (var key in participants) {
					if (participants[key] == otherUsername) {
						otherUsernameFound = true;
						otherSocketID = key;
						break;
					}
				}
				if (otherUsernameFound) {
					parsed.content = escape(parsed.content);
					parsed.created_at = new Date();
					parsed.uid = socket.id;
					io.sockets.connected[socket.id].emit('chat message',  JSON.stringify(parsed));
					parsed.content = "*** Direct message from " + participants[socket.id] + ": " + escape(directMessage);
					io.sockets.connected[otherSocketID].emit('chat message', JSON.stringify(parsed));
				}
				else {
					io.sockets.connected[socket.id].emit("whoops", JSON.stringify({"error_code":1,"message":"Your direct message could not be sent because other user is not online."}));
				}
			}
			else {
				parsed.content = escape(parsed.content);
				parsed.created_at = new Date();
				db.collection('messages', function(error, coll) {
					coll.insert(parsed, function(error, saved) {});
				});
				parsed.uid = socket.id; // no need to store socket ID in database
				// I am not emitting inside of coll.insert() for performance reasons
				// I could care less about every message being stored correctly
				io.emit('chat message', JSON.stringify(parsed));
			}
		}
		else {
			io.sockets.connected[socket.id].emit("whoops", JSON.stringify({"error_code":2,"message":"Your message was not sent. Perhaps you were disconnected from the room ungracefully (e.g., refreshed browser?)"}));
		}
	});

	socket.on('disconnect', function() {
		delete participants[socket.id];
		io.emit('a disconnect', Object.keys(participants).length);
	});
});

http.listen(process.env.PORT || 3000, function() {
	console.log('listening...');
});