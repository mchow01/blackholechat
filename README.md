# Overview
This is my first foray with Socket.io: yet another chat room.  Features:

1. "Unique" usernames
2. Count new users and disconnected users
3. Scrub usernames and messages before they are emitted to everyone (e.g., to prevent XSS and other garbage).
4. Limited emission from client-side: only username and content are emitted.
5. Detects ungraceful disconnection (e.g., reload of web browser)
6. Session storage for username and ID
7. One textbox for entry
8. Chat bubbles via CSS: green for you, light blue for others, gray for disconnected users
9. Mobile friendly (although it does not work on iOS Safari private mode, see http://stackoverflow.com/questions/21159301/quotaexceedederror-dom-exception-22-an-attempt-was-made-to-add-something-to-st)
10. Messages are stored into MongoDB
11. JSON GET API of last 10 messages
12. Private and unlogged messages to another person

# Live URL
https://blackholechat.herokuapp.com/

# References Used
* http://socket.io/get-started/chat/
* http://bower.io/#getting-started
* https://github.com/stephenplusplus/grunt-wiredep
* http://gruntjs.com/sample-gruntfile
