module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		jshint: {
			files: ['Gruntfile.js', 'server.js', 'src/**/*.js'],
			options: {
				globals: {
					jQuery: true,
					console: true,
					module: true
				}
			}
		},
		uglify: {
			dist: {
				files: {
					'public/chat.min.js': ['src/chat.js']
				}
			}
		},
		wiredep: {
			task: {
				// Point to the files that should be updated when
		    	// you run `grunt wiredep`
		    	src: [
		    		'public/index.html'   // .html support
		    		],
		    	options: {
		    	}
		    }
		}
	});
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-wiredep');
	grunt.registerTask('default', ['jshint', 'uglify', 'wiredep']);
};
