var socket = io();

socket.on('chat message', function(raw) {
	var msg = JSON.parse(raw);
	if (msg.username == sessionStorage.username && msg.uid == sessionStorage.uid) {
		$('#messages').append('<p><span class="username">' + msg.username + '</span> <span class="messageme">' + msg.content + '</span></p>');
	}
	else {
		$('#messages').append('<p><span class="username">' + msg.username + '</span> <span class="messageother">' + msg.content + '</span></p>');		
	}
	$("#messages").scrollTop($("#messages")[0].scrollHeight);
});

socket.on('new connection', function(raw) {
	$('#participants').html(raw);
});

socket.on('a disconnect', function(raw) {
	$('#participants').html(raw);
});

function updateGreetings() {
	$("#greetings").html('Hello ' + sessionStorage.username + '! Send private unlogged messages to another user starting with "@".');
}

socket.on('whoops', function(raw) {
	var error = JSON.parse(raw);
	alert(error.message);
	if (error.error_code == 2) {
		sessionStorage.clear();
		location.reload();
	}
});

socket.on('uid', function(raw) {
	parsed = JSON.parse(raw);
	sessionStorage.username = parsed.username;
	sessionStorage.uid = parsed.uid;
	updateGreetings();
});


$(document).ready(function() {
	// Compatibility check
	if (typeof(sessionStorage) === undefined || typeof(socket) === undefined) {
		$("#infobox").html('<p class="note">Sorry, your web browser does support real-time chat.</p>');
	}
	else {
		if (sessionStorage.username !== undefined && sessionStorage.uid !== undefined) {
			updateGreetings();
			$("#username").remove();
		}
		$.getJSON("/last.json").done(function(results) {
			results.forEach(function(msg) {
				$('#messages').append('<p><span class="username">' + msg.username + '</span> <span class="messageold">' + msg.content + '</span></p>');
			});
		});
	}
});

$("input#msgbox").keydown(function(event) {
	if ((event.keyCode == 13 || event.which == 13) && sessionStorage.username === undefined && sessionStorage.uid === undefined) {
		event.preventDefault();
		sessionStorage.username = $("input#msgbox").val();
		socket.emit('new user', sessionStorage.username);
		$("#username").remove();
		$("input#msgbox").val("");
	}
	else if ((event.keyCode == 13 || event.which == 13) && sessionStorage.username && sessionStorage.uid) {
		event.preventDefault();
		var msg = {"username":sessionStorage.username,"content":$("input#msgbox").val()};
		socket.emit('chat message', JSON.stringify(msg));
		$("input#msgbox").val("");
	}
});

